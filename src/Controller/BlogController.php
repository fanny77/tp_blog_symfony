<?php

namespace App\Controller;

use App\Entity\Articles;
use App\Form\ArticleType;
use App\Repository\ArticlesRepository;
use Symfony\Component\Filesystem\Path;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog/article/{id}", name="show_article", requirements= {"id"="\d+"})
     */
    public function show(ArticlesRepository $repo, $id): Response
    {

        $post = $repo->find($id);

        return $this->render('blog/article.html.twig', [
            'post' => $post
        ]);
    }

     /**
     * @Route("/blog/article/add", name="add_article")
     */
    public function addArticle(Request $request, ManagerRegistry $entityManager) : Response {
        
        $post = new Articles;
        $form = $this->createForm(ArticleType::class, $post);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $post = $form->getData();
            /** @var UploadedFile */
            $image = $form->get('image')->getData();
            
            $image->move(
                $this->getParameter('images_directory'),
                $image->getClientOriginalName()
            );

            //On ajoute l'image à l'objet 
            $post->setImage($image->getClientOriginalName());

            $entityManager->getManager()->persist($post);
            $entityManager->getManager()->flush();


            $this->addFlash(
                'notice',
                'Article ajouté avec succès'
            );

            return $this->redirectToRoute('home');
        }

        return $this->renderForm('blog/add.html.twig', [
            'form' => $form 
        ]);
    }

    /**
     * @Route("/blog/article/delete/{id}", name="delete_article", requirements={"id": "\d+"})
     */
    public function deleteArticles(ArticlesRepository $repoArticle, ManagerRegistry $entityManager, $id) : Response {
        
        $post = $repoArticle->find($id);

        $entityManager->getManager()->remove($post);
        $entityManager->getManager()->flush();

        
        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/blog/article/update/{id}", name="update_article", requirements={"id": "\d+"})
     */
    public function updateArticle(ArticlesRepository $repoArticle, ManagerRegistry $entityManager, $id, Request $request) : Response {
        
        $post = $repoArticle->find($id);
        if (!$post) {
            throw $this->createNotFoundException('Aucun contact n\'a été trouvé pour ' .$id);
        }
    
        $form = $this->createForm(ArticleType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var UploadedFile */
            $image = $form->get('image')->getData();

            $image->move(
                $this->getParameter('images_directory'),
                $image->getClientOriginalName()
            );

            //On ajoute l'image à l'objet 
            $post->setImage($image->getClientOriginalName());

            $entityManger = $entityManager->getManager();

            $entityManger->flush();

            $this->addFlash(
                'notice',
                'Article modifié avec succès'
            );

            return $this->redirectToRoute('home');
        }

        return $this->renderForm('blog/update.html.twig', [
            'form' => $form 
        ]);
    }
}
