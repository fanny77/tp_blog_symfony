<?php

namespace App\Controller;

use App\Repository\ArticlesRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(ArticlesRepository $repoArticle): Response
    {
        $posts = $repoArticle->findAll();

        return $this->render('default/index.html.twig', [
            'posts' => $posts,
        ]);
    }

}
